package cz.henzigo.a5_in_row.activities;


import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import cz.henzigo.a5_in_row.utils.GameBoard;

public class GameActivity extends AppCompatActivity {
    GameBoard gameView;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        gameView = new GameBoard(this);
        gameView.setBackgroundColor(Color.WHITE);
        setContentView(gameView);

    }
}
