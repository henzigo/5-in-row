package cz.henzigo.a5_in_row.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Canvas;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;

import cz.henzigo.a5_in_row.R;
import cz.henzigo.a5_in_row.service.BackgroundSoundService;

public class MainActivity extends AppCompatActivity {

    private Canvas canvas;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button play = (Button) findViewById(R.id.button);
        Button score = (Button) findViewById(R.id.button3);
        Button settings = (Button) findViewById(R.id.button2);

        play.setOnClickListener(v -> {
            Intent intent = new Intent(MainActivity.this, GameActivity.class);
            startActivity(intent);
        });/*
        score.setOnClickListener(v -> {
            Intent intent = new Intent(MainActivity.this, ScoreActivity.class);
            startActivity(intent);
        });*/
        settings.setOnClickListener(v -> {
            Intent intent = new Intent(MainActivity.this, SettingsActivity.class);
            startActivity(intent);
        });

        SharedPreferences mPrefs = getSharedPreferences("settings", 0);
        Integer mString = mPrefs.getInt("sound", 1);

        if(mString == 1) {
            Intent svc = new Intent(this, BackgroundSoundService.class);
            startService(svc);
        }
    }
}
