package cz.henzigo.a5_in_row.activities;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.widget.CheckBox;

import cz.henzigo.a5_in_row.R;

public class SettingsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        CheckBox sound = (CheckBox) findViewById(R.id.checkBox);
        CheckBox music = (CheckBox) findViewById(R.id.checkBox2);

        SharedPreferences mPrefs = getSharedPreferences("settings", 0);
        Integer mString = mPrefs.getInt("sound", 1);
        SharedPreferences mPrefs2 = getSharedPreferences("settings", 0);
        Integer mString2 = mPrefs2.getInt("music", 1);

        if(mString2 == 1) {
            music.setChecked(true);
        }

        if(mString == 1) {
            sound.setChecked(true);
        }


        sound.setOnTouchListener((v, event) -> {
            if(event.getAction() == MotionEvent.ACTION_DOWN){

                SharedPreferences mPrefs1 = getSharedPreferences("settings", 0);
                Integer mString1 = mPrefs1.getInt("sound", 1);

                if(sound.isChecked() == true) {
                    SharedPreferences.Editor editor = mPrefs1.edit();
                    editor.putInt("sound", 0).commit();

                    sound.setChecked(false);
                } else {
                    SharedPreferences.Editor editor = mPrefs1.edit();
                    editor.putInt("sound", 1).commit();

                    sound.setChecked(true);
                }

                //show alert
                return true; //this will prevent checkbox from changing state
            }
            return false;
        });

        music.setOnTouchListener((v, event) -> {
            if(event.getAction() == MotionEvent.ACTION_DOWN){

                SharedPreferences mPrefs1 = getSharedPreferences("settings", 0);
                Integer mString1 = mPrefs1.getInt("music", 1);

                if(music.isChecked() == true) {
                    SharedPreferences.Editor editor = mPrefs1.edit();
                    editor.putInt("music", 0).commit();

                    music.setChecked(false);
                } else {
                    SharedPreferences.Editor editor = mPrefs1.edit();
                    editor.putInt("music", 1).commit();

                    music.setChecked(true);
                }

                //show alert
                return true; //this will prevent checkbox from changing state
            }
            return false;
        });
    }
}
