package cz.henzigo.a5_in_row.utils;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.media.AudioManager;
import android.media.ToneGenerator;
import android.preference.PreferenceManager;
import android.view.MotionEvent;
import android.view.SurfaceView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import cz.henzigo.a5_in_row.activities.ScoreActivity;


/**
 * Created by info on 09.12.2016.
 */
public class GameBoard extends SurfaceView {

    private int tileSize = 177;
    private int rows;
    private int cols;

    GameLogic gameLogic;
    Paint paint;
    Canvas canvas;
    Context context;
    int canvasHeight;

    Gson gson = new Gson();

    SharedPreferences preferences;
    SharedPreferences.Editor editor;


    public GameBoard(Context context) {
        super(context);
        this.context = this.getContext();
        this.paint = new Paint();

        preferences = PreferenceManager.getDefaultSharedPreferences(this.context);
    }

    @SuppressLint("DrawAllocation")
    @Override
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        this.canvas = canvas;
        cols = canvas.getWidth() / tileSize;
        rows = canvas.getHeight() / tileSize;
        canvasHeight = canvas.getHeight() - 250;

        if(this.gameLogic == null)
            this.gameLogic = new GameLogic(rows, cols);

        paint();
    }

    @Override
    public boolean onTouchEvent(MotionEvent e) {
        if(e.getAction() == MotionEvent.ACTION_DOWN)
            this.move(e);

        invalidate();
        return true;
    }

    public void paint() {
        this.paint.setColor(Color.TRANSPARENT);
        this.canvas.drawRect(0, 0, this.canvas.getWidth(), canvasHeight, this.paint);
        this.paint.setStrokeWidth(2);
        this.paint.setColor(Color.BLACK);

        int centerX = (canvasHeight % this.tileSize) / 2;
        int centerY = (this.canvas.getWidth() % this.tileSize) / 2;

        for(int i = 0 ; i <= canvas.getWidth() ; i += this.tileSize){
            this.canvas.drawLine(i + centerY, centerX, i + centerY, (canvasHeight - (canvasHeight % this.tileSize)) + centerX, this.paint);
        }

        for(int i = 0 ; i <= canvas.getHeight() ; i += this.tileSize){
            this.canvas.drawLine(centerY, i + centerX, (this.canvas.getWidth() - (this.canvas.getWidth() % this.tileSize)) + centerY, i + centerX, this.paint);
        }

        for (int r = 0; r < this.rows; r++) {
            for (int c = 0; c < this.cols; c++) {
                int x = c * this.tileSize;
                int y = r * this.tileSize + 108 + centerX;

                int player = this.gameLogic.getCurrentState(r, c);
                if (player != 0) {
                    this.paint.setColor(Color.BLACK);
                    this.paint.setStrokeWidth(5);
                    this.paint.setStyle(Paint.Style.FILL);
                    this.paint.setTypeface(Typeface.create("Arial", Typeface.NORMAL));
                    this.paint.setTextSize(Float.valueOf(String.valueOf(150.0)));

                    if(player == 1) {
                        this.canvas.drawText("X", x + 50, y + 20, this.paint);
                    } else {
                        this.canvas.drawText("O", x + 50, y + 20, this.paint);
                    }
                }
            }
        }
    }

    public void move(MotionEvent e) {
        int centerX = (canvasHeight % this.tileSize) / 2;
        int centerY = (this.canvas.getWidth() % this.tileSize) / 2;

        int col = (int)((int)e.getX() - 50 + centerX) / (int)this.tileSize;
        int row = (int)((int)e.getY() - 84 + centerY) / (int)this.tileSize;

        if(this.cols > col && this.rows > row) {
            boolean checkGameEnd = this.gameLogic.checkGameStatus() != 0;
            int currentPosition = this.gameLogic.getCurrentState(row, col);

            if(!checkGameEnd && currentPosition == 0) {
                this.gameLogic.move(row, col);
                paint();
                AlertDialog alert;
                switch(this.gameLogic.checkGameStatus()) {
                    case 1:
                        alert = new AlertDialog.Builder(this.context).create();
                        alert.setTitle("Konec hry!");
                        alert.setMessage("Hráč 1 vyhrál během " + this.gameLogic.getMoves() + " tahů!");
                        alert.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                                (dialog, which) -> {
                                    dialog.dismiss();
                                    List<ScoreItem> scoreItems = gson.fromJson(
                                            preferences.getString(ScoreActivity.SCORE, ""),
                                            new TypeToken<List<ScoreItem>>() {
                                            }.getType()
                                    );

                                    if(scoreItems == null) {
                                        scoreItems = new ArrayList<>();
                                    }

                                    ScoreItem scoreItem = new ScoreItem();
                                    scoreItem.name = "Player 1";
                                    scoreItem.score = this.gameLogic.getMoves();
                                    scoreItem.dateTime = new Date();

                                    scoreItems.add(scoreItem);

                                    editor = preferences.edit();
                                    editor.putString(ScoreActivity.SCORE, gson.toJson(scoreItems));
                                    editor.apply();
                                });
                        alert.show();
                        break;
                    case 2:
                        alert = new AlertDialog.Builder(this.context).create();
                        alert.setTitle("Konec hry!");
                        alert.setMessage("Hráč 2 vyhrál během " + this.gameLogic.getMoves() + " tahů!");
                        alert.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                                (dialog, which) -> {
                                    dialog.dismiss();
                                    List<ScoreItem> scoreItems = gson.fromJson(
                                            preferences.getString(ScoreActivity.SCORE, ""),
                                            new TypeToken<List<ScoreItem>>() {
                                            }.getType()
                                    );

                                    if(scoreItems == null) {
                                        scoreItems = new ArrayList<>();
                                    }

                                    ScoreItem scoreItem = new ScoreItem();
                                    scoreItem.score = this.gameLogic.getMoves();
                                    scoreItem.name = "Player 2";
                                    scoreItem.dateTime = new Date();

                                    scoreItems.add(scoreItem);

                                    editor = preferences.edit();
                                    editor.putString(ScoreActivity.SCORE, gson.toJson(scoreItems));
                                    editor.apply();

                                });
                        alert.show();
                        break;
                    case -1:
                        alert = new AlertDialog.Builder(this.context).create();
                        alert.setTitle("Konec hry!");
                        alert.setMessage("Nepodařilo se vyhrát ani jednomu hráči!");
                        alert.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                                (dialog, which) -> {
                                    dialog.dismiss();
                                });
                        alert.show();
                        break;
                    default:
                        break;
                }
            } else {
                ToneGenerator toneG = new ToneGenerator(AudioManager.STREAM_ALARM, 50);
                toneG.startTone(ToneGenerator.TONE_CDMA_ALERT_CALL_GUARD, 200);
            }

            this.gameLogic.printAll();
        }
    }
}
