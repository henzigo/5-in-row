package cz.henzigo.a5_in_row.utils;

/**
 * Created by info on 09.12.2016.
 */
public class GameLogic implements LogicInterface {

    private int Rows;
    private int Cols;

    private int[][] board;      // The board values.
    private int     nextPlayer; // The player who moves next.
    private int     moves = 0;

    public GameLogic(int Rows, int Cols) {
        this.Rows = Rows;
        this.Cols = Cols;
        this.board = new int[this.Rows][this.Cols];
        this.reset();
    }

    public int nextPlayer() {
        return this.nextPlayer;
    }

    public int checkGameStatus() {
        int row, col;

        for (row = 0; row < this.Rows; row++) {
            for (col = 0; col < this.Cols; col++) {
                int player = this.board[row][col];
                if (player != 0) {
                    if (row < this.Rows - 4) // |
                        if (this.checkWin(row, 1, col, 0)) return player;

                    if (col < this.Cols - 4) { // -
                        if (this.checkWin(row, 0, col, 1))  return player;

                        if (row < this.Rows - 4) { // \
                            if (this.checkWin(row, 1, col, 1)) return player;
                        }
                    }

                    if (col > 3 && row < this.Rows - 4) // /
                        if (this.checkWin(row, 1, col, -1)) return player;
                }
            }
        }

        if (this.moves == this.Rows * this.Cols) return -1;
        else return 0;
    }

    public int getMoves() {
        return this.moves;
    }

    private boolean checkWin(int row, int dir_r, int col, int dir_c) {
        int player = this.board[row][col];
        for (int i = 1; i < 5; i++) {
            if (this.board[row + dir_r * i][col + dir_c * i] != player) return false;
        }
        return true;
    }

    public int getCurrentState(int row, int col) {
        return this.board[row][col];
    }

    public void reset() {

        for (int i = 0; i < this.Rows; i++) {
            for (int j = 0; j < this.Cols; j++) {
                this.board[i][j] = 0;
            }
        }

        this.nextPlayer = 1;
        this.moves = 0;

    }

    public void move(int row, int col) {
        assert this.board[row][col] == 0;
        this.board[row][col] = this.nextPlayer;
        this.nextPlayer = (this.nextPlayer() == 1 ? 2 : 1);
        this.moves += 1;
    }

    public void printAll() {
        System.out.println();
        for (int i = 0; i < this.Rows; i++) {
            for (int j = 0; j < this.Cols; j++) {
                System.out.print(this.board[i][j] + " ");
            }
            System.out.println();
        }
    }
}
