package cz.henzigo.a5_in_row.utils;

/**
 * Created by info on 11.12.2016.
 */
public interface LogicInterface {
    int nextPlayer();
    int checkGameStatus();
    int getMoves();
    int getCurrentState(int row, int col);
    void reset();
    void move(int row, int col);
    void printAll();
}
