package cz.henzigo.a5_in_row.utils;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by henzigo on 21/12/2017.
 */

public class ScoreItem implements Serializable {
    public Date dateTime;
    public String name;
    public int score;
}

